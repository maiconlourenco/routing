import React, { useState, useEffect } from "react";
import CharacterList from "../CharacterList";
import "./styles.css";

const PageOne = () => {
  const [characterList, setCharacterList] = useState([]);
  const [nextUrl, setNextUrl] = useState(
    "https://rickandmortyapi.com/api/character/"
  );
  const [prevPage, setprevPage] = useState("");
  const [nextPage, setNextPage] = useState("");
  const [count, setCount] = useState(20);

  useEffect(() => {
    if (nextUrl)
      fetch(nextUrl)
        .then((res) => res.json())
        .then((body) => {
          setCharacterList([...body.results]);

          if (!nextPage) {
            setCount(count - characterList.length);
          } else {
            setCount(count + [...body.results].length);
          }
          setprevPage(body.info.prev);
          setNextPage(body.info.next);
        });
  }, [nextUrl]);

  const handleNextPage = () => {
    if (nextPage !== null) {
      setprevPage(false);
      setNextUrl(nextPage);
    }
  };

  const handlePrevPage = () => {
    if (prevPage !== null) {
      setNextPage(false);
      setNextUrl(prevPage);
    }
  };

  return (
    <div className="App">
      <header className="page1-content">
        <p>Foram carregados {count} personagens!</p>
        <div id="buttons">
          <button onClick={handlePrevPage}>Previous</button>
          <button onClick={handleNextPage}>Next</button>
        </div>
        <CharacterList list={characterList} range={0} />
      </header>
    </div>
  );
};

export default PageOne;
