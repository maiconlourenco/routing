import React from "react";
import Character from "../Character";
import "./styles.css";
import { starWarsImages } from "./helper.js";

const CharacterList = (props) => {
  let indexImg = props.range - 1;
  return (
    <div className="CharacterList">
      {props.list.map((character, index) => {
        indexImg += 1;
        return (
          <Character
            key={index}
            name={character.name}
            gender={character.gender}
            image={
              character.image !== undefined
                ? character.image
                : starWarsImages[indexImg]
            }
          />
        );
      })}
    </div>
  );
};

export default CharacterList;
