import React from "react";
import "./styles.css";

const Character = (props) => {
  return (
    <div className="character">
      <img src={props.image} alt="characterIcon" />
      <h1>Nome: {props.name}</h1>
      <p>Gender: {props.gender}</p>
    </div>
  );
};

export default Character;
