import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import PageOne from "./components/PageOne";
import PageTwo from "./components/PageTwo";
import "./App.css";

function App() {
  return (
    <div>
      <div className="navbar">
        <ul className="navbar-pages">
          <li>
            <Link id="link" to="/">
              Page One
            </Link>
          </li>
          <li>
            <Link id="link" to="/page-two">
              Page Two
            </Link>
          </li>
        </ul>
      </div>

      <div className="page-content">
        <Switch>
          <Route exact path="/">
            <PageOne />
          </Route>
          <Route path="/page-two">
            <PageTwo />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
